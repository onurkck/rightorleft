//
//  RoLTests.swift
//  RoLTests
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import XCTest
@testable import RoL

class RoLTests: XCTestCase {
    var rolService:RoLServices!
    override func setUp() {
        super.setUp()
        rolService = ParseRoLServices()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    //MARK: - Login and Sign Up Test Functions
    
    func testLoginUser(){
        rolService.loginWithUserName("onurkucuk", password: "123456") { (returnedUser:User?, error:NSError?) -> Void in
            if let parseError = error{
                print(parseError.description)
            }
            else{
                print("Login successful")
            }
        }
    }
    
    func testLogoutUser(){
        rolService.logoutUser(createUserObject()) { (success:Bool?, error:NSError?) -> Void in
            if let parseError = error{
                print(parseError.description)
            }
            else{
                print("Logout was successful")
            }
        }
    }
    
    func testSignUpUser(){
        rolService.signUpUser("onurkucuk", email: "onurkucuk@dorianlabs.com", password: "123456") { (returnUser:User?, error:NSError?) -> Void in
            if let parseError = error{
                print(parseError.description)
            }
            else{
                print("Sign up successfull")
            }
        }
    }
    
    //MARK: - Post Related Test Functions
    
    func testGetPostForAUser(){
//        rolService.getPostsForAUser(<#T##recieverUser: User##User#>, completionFunction: <#T##MultiplePostReturnFunction##MultiplePostReturnFunction##([Post]?, NSError?) -> Void#>)    
    }
    
    
    //MARK: - Helper Methods
    
    func createUserObject() -> User{
        let userObj = User()
        userObj.userName = "onurkucuk"
        userObj.userEmail = "onurkucuk91@gmail.com"
        userObj.userId = ""
        
        return userObj
    }
}
