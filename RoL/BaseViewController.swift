//
//  BaseViewController.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UITextFieldDelegate {

    var rolService:RoLServices!
    var userDefaults:NSUserDefaults!
    var loaderVC:LoaderVC?
    var currentUser:User?
    override func viewDidLoad() {
        super.viewDidLoad()

        rolService = ParseRoLServices()
        
        if UserSession.getInstance().myUser != nil && currentUser == nil {
            currentUser = UserSession.getInstance().myUser
        }
    }

    func showErrorMessage(error:NSError){
        
        let alertcontroller:UIAlertController = UIAlertController(title: "Sorry!", message: error.userInfo["NSLocalizedDescription"] as? String, preferredStyle: UIAlertControllerStyle.Alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        self.presentViewController(alertcontroller, animated: true, completion: nil)
    }
    
    func showAlertControllerWithTitle(title:String, message:String) {
        let alertcontroller:UIAlertController = UIAlertController(title: NSLocalizedString(title, comment: "title"), message:NSLocalizedString(message, comment: "message"), preferredStyle: UIAlertControllerStyle.Alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        self.presentViewController(alertcontroller, animated: true, completion: nil)
    }
    
    func showLoaderViewWithMessage(message:String){
        loaderVC = LoaderVCFactory.getLoaderView()
        loaderVC?.titleText = NSLocalizedString(message, comment: "loader message")
        self.presentViewController(loaderVC!, animated: true, completion: nil)
    }
    
    func dismissLoaderVC(completionFunction:()->Void){
        loaderVC?.dismissViewControllerAnimated(true, completion: completionFunction)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func showActivityIndicator(activityIndicator:UIActivityIndicatorView){
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()

    }
    
    func dismissActivityIndicator(activityIndicator:UIActivityIndicatorView){
        activityIndicator.stopAnimating()
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }
    // MARK: - Navigation
    
    func goToMainScreen(){
        
        let mainScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("ScrollerVC") as! ScrollerVC
        presentViewController(mainScreenVC, animated: true, completion: nil)
    }
    
    func setCurrentPageForInboxScreen(){
        UserSession.getInstance().currentPage = 0
    }
    
    func setCurrentPageForCameraScreen(){
        UserSession.getInstance().currentPage = 1
    }
    
    func setCurrentPageForProfileScreen(){
        UserSession.getInstance().currentPage = 2
    }
}
