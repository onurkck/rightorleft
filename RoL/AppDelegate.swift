//
//  AppDelegate.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Parse.setApplicationId("UbAZOKhsoqKxfMblInrJIBc1PSf6M231rNzYzPK7", clientKey: "2HPUFRsd9Y8QHEs6KXPf4VRhQHpPZA7Rc4I1ahCk")
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        var initialViewController:UIViewController!
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        
        if PFUser.currentUser() != nil {
            let currentInstallation = PFInstallation.currentInstallation()
            PFUser.currentUser()!["device"] = currentInstallation
            PFUser.currentUser()?.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                print("device saving status : \(success)")
            })
            
            initialViewController = storyBoard.instantiateViewControllerWithIdentifier("ScrollerVC") as! ScrollerVC
            UserSession.getInstance().myUser = User.initWithParseObject(PFUser.currentUser()!)
            let rolService:RoLServices = ParseRoLServices()
            rolService.getUserAllFriends(UserSession.getInstance().myUser, completionFunction: { (returnedUserList:[User]?, error:NSError?) -> Void in
                if returnedUserList != nil {
                    UserSession.getInstance().myUserFriendLists = returnedUserList
                }
            })
        }
        else {
            initialViewController = storyBoard.instantiateViewControllerWithIdentifier("LoginFlowNav") as! UINavigationController
        }
        self.window?.rootViewController = initialViewController
        
//        let userNotificationTypes = (UIUserNotificationType.Alert, UIUserNotificationType.Badge,  UIUserNotificationType.Sound)
        
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.respondsToSelector("backgroundRefreshStatus")
            let oldPushHandlerOnly = !self.respondsToSelector("application:didReceiveRemoteNotification:fetchCompletionHandler:")
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
        
        if application.respondsToSelector("registerUserNotificationSettings:") {
            let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        } else {
            application.registerForRemoteNotificationTypes([.Badge, .Alert, .Sound])
        }
        
        
//        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
//        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
//        application.registerForRemoteNotifications()
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let currentInstallation : PFInstallation! = PFInstallation.currentInstallation()
        currentInstallation.setDeviceTokenFromData(deviceToken)
        currentInstallation.addUniqueObject("Global", forKey: "channels")
        currentInstallation.saveInBackgroundWithBlock(nil)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
//        PFPush.handlePush(userInfo)
        
        PFPush.handlePush(userInfo)
        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
        }
    }
}

