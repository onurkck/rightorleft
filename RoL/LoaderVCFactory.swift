//
//  LoaderVCFactory.swift
//  RoL
//
//  Created by Onur Küçük on 2.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class LoaderVCFactory: NSObject {

    static func getLoaderView() -> LoaderVC{
        var loaderVC = LoaderVC()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        loaderVC = storyboard.instantiateViewControllerWithIdentifier("LoaderVC") as! LoaderVC
        loaderVC.modalTransitionStyle = .CoverVertical
        loaderVC.modalPresentationStyle = .FormSheet
        loaderVC.preferredContentSize = CGSizeMake(150, 150)
        return loaderVC
    }
}
