//
//  PostCollectionViewCell.swift
//  RoL
//
//  Created by Onur Küçük on 6.12.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class PostCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var leftRate: UILabel!
    @IBOutlet weak var rightRate: UILabel!
}
