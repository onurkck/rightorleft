//
//  Post.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit
import Parse
class Post: NSObject {
    
    var toGroup:String!
    var postID:String!
    var senderUser:User!
    var leftImageFile:PFFile!
    var rightImageFile:PFFile!
    var rightImage:UIImage!
    var leftImage:UIImage!
    var rightRate:Int!
    var leftRate:Int!
    var parseObject:PFObject!
    
    static func initWithParseObject(parseObject:PFObject) -> Post {
        let newPost = Post()
        newPost.postID = parseObject.objectId
        newPost.toGroup = parseObject.objectId //TODO - This is revised when database will be created on Parse
        newPost.senderUser = User.initWithParseObject(parseObject.objectForKey("senderUser") as! PFUser)
        newPost.rightImageFile = parseObject.objectForKey("rightImage") as! PFFile
        newPost.leftImageFile = parseObject.objectForKey("leftImage") as! PFFile
        newPost.rightRate = parseObject.objectForKey("rightRate") as! Int
        newPost.leftRate = parseObject.objectForKey("leftRate") as! Int
        newPost.parseObject = parseObject
        return newPost
    }
    
    func convertToParseObject(postObject:Post) -> PFObject {
        var convertedLeftImageFile:PFFile?
        var convertedRightImageFile:PFFile?
        postObject.parseObject.setObject(senderUser.parseObject, forKey: "senderUser")
        postObject.parseObject.setObject(toGroup!, forKey: "toGroup")
        convertedLeftImageFile = PFFile(name: "leftImage.png", data: convertUIImageToNSData(leftImage))
        convertedRightImageFile = PFFile(name: "rightImage.png", data: convertUIImageToNSData(rightImage))
        postObject.parseObject.setObject(convertedLeftImageFile!, forKey: "leftImage")
        postObject.parseObject.setObject(convertedRightImageFile!, forKey: "rightImage")
        postObject.parseObject.setObject(leftImageFile, forKey: "leftImage")
        postObject.parseObject.setObject(rightImageFile, forKey: "rightImage")
        postObject.parseObject.setObject(rightRate, forKey: "rightRate")
        postObject.parseObject.setObject(leftRate, forKey: "leftRate")
        return postObject.parseObject
    }
    
    //MARK: - Helper Methods
    
    func convertUIImageToNSData(image:UIImage) -> NSData {
        return UIImageJPEGRepresentation(image, 0.3)!
    }
}
