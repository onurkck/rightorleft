//
//  UserSession.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit
import Parse

class UserSession {
    
    static var mySession:UserSession!
    var myUser:User!
    var myUserFriendLists:[User]!
    var myUserPostHistory:[Post]!
    var currentPage:Int!
    
    static func getInstance() -> UserSession {
        
        if mySession == nil {
            mySession = UserSession()
        }
        return mySession
    }
    
    func removeSession() {
        myUser = nil
        myUserFriendLists = nil
        myUserPostHistory = nil
        currentPage = nil
    }
}
