//
//  SignUpVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class SignUpVC: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var signUpTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var userNameTextField:UITextField!
    var userEmailTextField:UITextField!
    var userPasswordTextField:UITextField!
    var userRePasswordTextField:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationItem.title = "Sign Up"
    }
    //MARK: - Parse Service Methods
    func signUpWithUserInformations(userEmail:String, userName:String, userPassword:String){
        rolService.signUpUser(userName, email: userEmail, password: userPassword) { (returnedUser:User?, error:NSError?) -> Void in
            self.activityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                self.goToMainScreen()
            }
        }
    }
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        switch indexPath.row {
        case 0:
            cell = signUpTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            userNameTextField = cell.viewWithTag(1) as! UITextField
            userNameTextField.textAlignment = .Center
            userNameTextField.placeholder = "Username"
            userNameTextField.delegate = self
            textFieldShouldReturn(userNameTextField)
        case 1:
            cell = signUpTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            userEmailTextField = cell.viewWithTag(1) as! UITextField
            userEmailTextField.textAlignment = .Center
            userEmailTextField.placeholder = "Email"
            userEmailTextField.delegate = self
            textFieldShouldReturn(userEmailTextField)
        case 2:
            cell = signUpTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            userPasswordTextField = cell.viewWithTag(1) as! UITextField
            userPasswordTextField.textAlignment = .Center
            userPasswordTextField.placeholder = "Password"
            userPasswordTextField.secureTextEntry = true
            userPasswordTextField.delegate = self
            textFieldShouldReturn(userPasswordTextField)
        case 3:
            cell = signUpTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            userRePasswordTextField = cell.viewWithTag(1) as! UITextField
            userRePasswordTextField.textAlignment = .Center
            userRePasswordTextField.placeholder = "Please confirm password"
            userRePasswordTextField.secureTextEntry = true
            userRePasswordTextField.delegate = self
            textFieldShouldReturn(userRePasswordTextField)
        case 4:
            cell = signUpTableView.dequeueReusableCellWithIdentifier("CellWithButton", forIndexPath: indexPath)
            let signUpButton = cell.viewWithTag(1) as! UIButton
            signUpButton.setTitle("Sign Up", forState: .Normal)
        default:
            return signUpTableView.dequeueReusableCellWithIdentifier("CellWithButton", forIndexPath: indexPath)
        }
        return cell
    }
    
    //MARK: Button Action
    
    @IBAction func signUpButtonAction(sender: UIButton) {
        let userName = userNameTextField.text
        let userEmail = userEmailTextField.text
        let userPassword = userPasswordTextField.text
        let userRePassword = userRePasswordTextField.text
        
        if (userName == "" || userEmail == "" || userPassword == "" || userRePassword == ""){
            showAlertControllerWithTitle("Warning", message: "Please fill all the fields.")
        }
        else{
            if isValidEmail(userEmail!){
                if userPassword == userRePassword {
                    self.activityIndicator.hidden = false
                    self.activityIndicator.startAnimating()
                    UIApplication.sharedApplication().beginIgnoringInteractionEvents()
                    self.signUpWithUserInformations(userEmail!, userName: userName!, userPassword: userPassword!)
                }
                else{
                    showAlertControllerWithTitle("Warning", message: "Your passwords are not mathed.")
                }
            }
            else{
                showAlertControllerWithTitle("Warning", message: "Your email is not valid.")
            }
        }
    }
    
    
    //MARK: - Private Methods
    
    func isValidEmail(emailAdress:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(emailAdress)
    }
}
