//
//  RoLServices.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import Foundation
import Parse

typealias UserReturnFunction = (User? , NSError?) -> Void
typealias MultipleUserReturnFunction = ([User]?, NSError?) -> Void
typealias PostReturnFunction = (Post?, NSError?) -> Void
typealias MultiplePostReturnFunction = ([Post]?, NSError?) -> Void
typealias FriendRequestsReturnFunction = ([FriendRequest]?, NSError?) -> Void
typealias BooleanReturnFunction = (Bool?, NSError?) -> Void
typealias ImageReturnFunction = (UIImage?, NSError?) -> Void
typealias ResponseReturnFunction = (String?, NSError?) -> Void


protocol RoLServices {
    
    //MARK: - Login Functions
    
    func loginWithUserName(userName:String, password:String, completionFunction:UserReturnFunction)
    
    func signUpUser(userName:String, email:String, password:String, completionFunction:UserReturnFunction)
    
    func logoutUser(user:User, completionFunction:BooleanReturnFunction)
    
    func changePassword(user:User, completionFunction:BooleanReturnFunction)
    
    //MARK: - Post Related Functions
    
    func getPostsForAUser(recieverUser:User, completionFunction:MultiplePostReturnFunction)
    
    func getPostHistoryOfUser(currentUser:User, completionFunction:MultiplePostReturnFunction)
    
    func sendPostToUserList(post:Post, userList:[User], completionFunction:BooleanReturnFunction)
    
    func ratePostWithSide(ratingPost:Post, side:String, newRate:Int, completionFunction:BooleanReturnFunction)
    
    func deletePost(deletingPost:Post, completionFunction:BooleanReturnFunction)
    
    func convertImageFileToUIImage(imageFile:PFFile, completionFunction:ImageReturnFunction)
    
    //MARK: - Friends Related Functions
    
    func sendFriendRequesToUser(senderUser:User, recieverUserName:String, completionFunction:BooleanReturnFunction)
    
    func checkingFriendRequest(recieverUser:User, completionFunction:FriendRequestsReturnFunction)
    
    func responseFriendRequest(friendRequest:FriendRequest, response:String, completionFunction:BooleanReturnFunction)
    
    func getUserAllFriends(myUser:User, completionFunction:MultipleUserReturnFunction)
    
    
    
    
    
}
