//
//  SendPostVC.swift
//  RoL
//
//  Created by Onur Küçük on 8.12.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class SendPostVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var friendsTableView: UITableView!
    @IBOutlet weak var lefImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var creatingPost: Post!
    var myFriends:[User]!
    var toUserList = [User]()
    var userSelectionStat = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lefImageView.contentMode = .ScaleAspectFit
        self.rightImageView.contentMode = .ScaleAspectFit
        self.lefImageView.image = self.creatingPost.leftImage
        self.rightImageView.image = self.creatingPost.rightImage
        
        if UserSession.getInstance().myUserFriendLists != nil {
            self.myFriends = UserSession.getInstance().myUserFriendLists
        }
        else {
            getFriends()
        }
        self.prepareUserWithSelectionDict()
    }
    
    //MARK: - Rol Service
    func sendPostToUserList(){
        self.activityIndicator.hidden = false
        self.activityIndicator.startAnimating()
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
        rolService.sendPostToUserList(self.creatingPost, userList: self.toUserList) { (success:Bool?, error:NSError?) -> Void in
            
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            self.activityIndicator.stopAnimating()
            if success == true {
                //                self.showAlertControllerWithTitle("Success", message: "Your post was sent successfully.")
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                self.showAlertControllerWithTitle("Sorry", message: "There was an error occured.Please try again.")
            }
        }
    }
    func getFriends(){
        rolService.getUserAllFriends(currentUser!) { (userFriends:[User]?, error:NSError?) -> Void in
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                self.myFriends = userFriends!
                self.friendsTableView.reloadData()
            }
        }
    }
    
    //MARK: - Table View Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isThereFriends(){
            return 1
        }
        return self.myFriends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        if !isThereFriends(){
            return tableView.dequeueReusableCellWithIdentifier("noFriendsCell", forIndexPath: indexPath)
        }
        else {
            cell = configureUserCellWithIndexPath(indexPath)
        }
        return cell
    }
    
    func configureUserCellWithIndexPath(indexPath:NSIndexPath) -> UITableViewCell{
        let cell = self.friendsTableView.dequeueReusableCellWithIdentifier("userCell", forIndexPath: indexPath) as! SendPostTableViewCell
        let userName = self.myFriends[indexPath.row].userName
        cell.userNameLabel.text = userName
        cell.checkButton.tag = indexPath.row
        let isChecked = self.userSelectionStat[indexPath.row]
        if isChecked {
            cell.checkButton.setImage(UIImage(named: "check"), forState: .Normal)
        }
        else {
            cell.checkButton.setImage(UIImage(named: "unchecked"), forState: .Normal)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectOrUnselectUserWithIndex(indexPath.row)
        self.friendsTableView.reloadData()
    }
    
    //MARK: - Actions
    @IBAction func userSelected(sender: UIButton) {
        self.selectOrUnselectUserWithIndex(sender.tag)
        self.friendsTableView.reloadData()
    }
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func sendPost(sender: UIBarButtonItem) {
        self.createToUserList()
        if self.toUserList.count > 0{
            self.sendPostToUserList()
        }
        else {
            self.showAlertControllerWithTitle("Warning", message: "Please select friend(s).")
        }
    }
    //MARK: - Private Methods
    func isThereFriends() -> Bool{
        return self.myFriends.count > 0
    }
    
    func prepareUserWithSelectionDict(){
        for _ in 0..<self.myFriends.count {
            self.userSelectionStat.append(false)
        }
    }
    
    func selectOrUnselectUserWithIndex(index:Int){
        if self.userSelectionStat[index] {
            self.userSelectionStat[index] = false
        }
        else {
            self.userSelectionStat[index] = true
        }
    }
    
    func createToUserList(){
        for var i = 0; i < self.userSelectionStat.count; i++ {
            let selectionStat = self.userSelectionStat[i]
            if selectionStat {
                self.toUserList.append(self.myFriends[i])
            }
        }
    }
}
