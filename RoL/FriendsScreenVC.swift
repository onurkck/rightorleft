//
//  FriendsScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 2.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class FriendsScreenVC: BaseViewController,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var friendsTableView: UITableView!
    
    var myFriends:[User]?
    var userNameArray:[String]!
    var friendRequests:[FriendRequest]?
    
    var userNameTextField:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNameArray = [String]()
        if UserSession.getInstance().myUserFriendLists == nil {
            getFriends()
        }
        else {
            myFriends = UserSession.getInstance().myUserFriendLists!
        }
        isThereAnyFriendRequest()
    }
    
    //MARK: - ROL Service Methods
    func getFriends(){
        rolService.getUserAllFriends(currentUser!) { (userFriends:[User]?, error:NSError?) -> Void in
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                self.myFriends = userFriends!
                self.friendsTableView.reloadData()
                for user in self.myFriends!{
                    self.userNameArray!.append(user.userName)
                }
            }
        }
    }
    
    func isThereAnyFriendRequest(){
        rolService.checkingFriendRequest(self.currentUser!) { (returnedFriendRequests:[FriendRequest]?, error:NSError?) -> Void in
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                self.friendRequests = returnedFriendRequests
                self.friendsTableView.reloadData()
            }
        }
    }
    
    func responseFriendRequest(selectedFriendRequest:FriendRequest, response:String){
        rolService.responseFriendRequest(selectedFriendRequest, response: response) { (success:Bool?, error:NSError?) -> Void in
            if success == true {
                self.friendsTableView.reloadData()
            }
            else{
                self.showErrorMessage(error!)
            }
        }
    }
    
    func sendFriendRequestToUser(userName:String){
        rolService.sendFriendRequesToUser(self.currentUser!, recieverUserName: userName) { (success:Bool?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else {
                self.showAlertControllerWithTitle(NSLocalizedString("Success", comment: ""), message: "Friend request was sent")
            }
        }
    }
    
    //MARK: - Table View Data Sources
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return NSLocalizedString("Friend Requests", comment: "")
        }
        else {
            return NSLocalizedString("Friends", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if isThereAFriendRequest(){
                return friendRequests!.count
            }
            else{
                return 1
            }
        }
        else{
            if isThereAFriend(){
                return myFriends!.count
            }
            else{
                return 1
            }
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        if indexPath.section == 0 {
            if isThereAFriendRequest(){
                cell = self.configureCellWithFriendRequest(indexPath)
            }
            else{
                cell = self.cellForInformation(NSLocalizedString("There is no friend request.", comment: ""), indexPath: indexPath)
            }
        }
        else{
            if isThereAFriend(){
                cell = self.configureCellWithUserFriend(indexPath)
            }
            else{
                cell = self.cellForInformation(NSLocalizedString("There is no friend", comment: ""), indexPath: indexPath)
            }
        }
        return cell
    }
    
    func configureCellWithFriendRequest(indexPath:NSIndexPath) -> UITableViewCell{
        let cell = friendsTableView.dequeueReusableCellWithIdentifier("FriendRequestCell", forIndexPath: indexPath)
        
        let declineRequestButton = cell.viewWithTag(1) as! UIButton
        let senderUserNameLabel = cell.viewWithTag(2) as! UILabel
        let acceptRequestButton = cell.viewWithTag(3) as! UIButton
        
        declineRequestButton.tag = indexPath.row
        acceptRequestButton.tag = indexPath.row
        
        senderUserNameLabel.text = self.friendRequests![indexPath.row].senderUser.userName
        declineRequestButton.addTarget(self, action: "declineRequestButtonPressed:", forControlEvents: .TouchUpInside)
        acceptRequestButton.addTarget(self, action: "acceptRequestButtonPressed:", forControlEvents: .TouchUpInside)
        
        return cell
    }
    
    func configureCellWithUserFriend(indexPath:NSIndexPath) -> UITableViewCell{
        let cell = friendsTableView.dequeueReusableCellWithIdentifier("FriendCell", forIndexPath: indexPath)
        let userNameLabel = cell.viewWithTag(1) as! UILabel
        userNameLabel.text = myFriends![indexPath.row].userName;
        
        return cell
    }
    
    func cellForInformation(information:String, indexPath:NSIndexPath) -> UITableViewCell{
        let cell = friendsTableView.dequeueReusableCellWithIdentifier("FriendCell", forIndexPath: indexPath)
        let infoLabel = cell.viewWithTag(1) as! UILabel
        infoLabel.text = NSLocalizedString(information, comment: "")
        return cell
    }
    
    //MARK: - Actions
    func declineRequestButtonPressed(sender:UIButton){
        self.responseFriendRequest(friendRequests![sender.tag], response: kDeclineFriendReq)
        friendRequests!.removeAtIndex(sender.tag)
    }
    
    func acceptRequestButtonPressed(sender:UIButton){
        self.responseFriendRequest(friendRequests![sender.tag], response: kAcceptFriendReq)
        self.myFriends?.append(friendRequests![sender.tag].senderUser)
        friendRequests!.removeAtIndex(sender.tag)
        self.friendsTableView.reloadData()
    }
    
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addButtonPressed(sender: UIBarButtonItem) {
        let alertController:UIAlertController = UIAlertController(title: NSLocalizedString("Add Friend", comment: ""), message: NSLocalizedString("Please enter the username to add.", comment: ""), preferredStyle:.Alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Add", style: .Default, handler: { (alertAction) -> Void in
            let userNameToAdd:String = self.userNameTextField.text!
            if userNameToAdd == "" {
                self.showAlertControllerWithTitle(NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("Please enter an username", comment: ""))
            }
            else if (self.userNameArray!.contains(userNameToAdd)) {
                self.showAlertControllerWithTitle(NSLocalizedString("Sorry", comment: ""), message: NSLocalizedString("You have already friend with \(userNameToAdd)", comment: ""))
            }
            else{
                self.sendFriendRequestToUser(userNameToAdd)
                
            }
        }))
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Username"
            self.userNameTextField = textField
        }
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    //MARK: - Private Methods
    
    func isThereAFriendRequest() -> Bool{
        return friendRequests?.count > 0
    }
    
    func isThereAFriend() -> Bool{
        return myFriends?.count > 0
    }
}
