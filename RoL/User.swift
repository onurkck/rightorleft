//
//  User.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit
import Parse

class User: NSObject {

    var userId:String?
    var userName:String!
    var userEmail:String!
    var deviceObject:PFInstallation!
    var parseObject:PFUser!
    
    static func initWithParseObject(parseObject:PFUser) -> User {
        let loggedInUser = User()
        loggedInUser.userId = parseObject.objectId!
        loggedInUser.userName = parseObject.objectForKey("username") as! String
        loggedInUser.userEmail = parseObject.objectForKey("email") as! String
        loggedInUser.deviceObject = parseObject.objectForKey("device") as! PFInstallation
        loggedInUser.parseObject = parseObject
        return loggedInUser
    }
    
    static func convertToParseObject(userObject:User) -> PFUser {
        userObject.parseObject.setObject(userObject.userName, forKey: "username")
        userObject.parseObject.setObject(userObject.userEmail, forKey: "email")
        return userObject.parseObject
    }
    
    
}
