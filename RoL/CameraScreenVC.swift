//
//  CameraScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import AVFoundation
import UIKit
import CoreMedia
import CoreImage

class CameraScreenVC: BaseViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var cameraPreview: UIView!
    @IBOutlet weak var capturedImage: UIImageView!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var takeFromGalleryButton: UIButton!
    @IBOutlet weak var retakeButton: UIButton!
    
    var isCapturedPhoto:Bool!
    var isFlashActivated:Bool!
    var isBackCamera:Bool!
    var isThereImage:Bool!
    var isReloadForTheNext:Bool!
    
    var takenImages = [UIImage]()
    var backCamera:AVCaptureDevice?
    var imageStillOutput:AVCaptureStillImageOutput = AVCaptureStillImageOutput()
    var imagePicker = UIImagePickerController()
    var tapToFocus:UITapGestureRecognizer!
    var creatingPost = Post()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareVC()
        
        if cameraPreview != nil {
            self.authorizeCamera()
            self.initializeCamera()
            
            tapToFocus = UITapGestureRecognizer(target: self, action: "focusCamera")
            cameraPreview.addGestureRecognizer(tapToFocus)
        }
    }
    
    
    
    //MARK: - Camera Functions
    func authorizeCamera() {
        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: {
            (granted: Bool) -> Void in
            // If permission hasn't been granted, notify the user.
            
            if !granted {
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.showAlertControllerWithTitle("Couldn't use camera", message: "This application does not have permission to use camera. Please update your privacy settings.")
                })
            }
        })
    }
    
    func initializeCamera(){
        let session:AVCaptureSession = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetiFrame1280x720
        
        let bounds=cameraPreview.bounds;
        let captureVideoPreviewLayer:AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        captureVideoPreviewLayer.bounds = bounds
        captureVideoPreviewLayer.frame = CGRectMake(0, 0, self.view.frame.width, self.cameraPreview.frame.size.height)

        cameraPreview.layer.addSublayer(captureVideoPreviewLayer)
        
        let view1:UIView = self.cameraPreview
        let viewLayer = view1.layer
        viewLayer.masksToBounds = true
        
        let devices:[AVCaptureDevice] = AVCaptureDevice.devices() as! [AVCaptureDevice]
        
        var frontCamera:AVCaptureDevice?
        
        for device:AVCaptureDevice in devices
        {
            
            NSLog(device.localizedName)
            if(device.hasMediaType(AVMediaTypeVideo))
            {
                if(device.position == AVCaptureDevicePosition.Back)
                {
                    if (device.hasTorch && device.hasFlash)
                    {
                        
                        backCamera = device
                        do {
                            try backCamera?.lockForConfiguration()
                        }
                        catch _ {}
                        
                        backCamera?.focusMode = AVCaptureFocusMode.AutoFocus
                        backCamera?.flashMode = AVCaptureFlashMode.Off
                        backCamera?.unlockForConfiguration()
                    }
                }
                else
                {
                    frontCamera = device
                }
            }
            
        }
        
        if(isBackCamera == true)
        {
            do {
                let input:AVCaptureDeviceInput = try AVCaptureDeviceInput(device: self.backCamera)
                session.addInput(input)
            }
            catch _ {}
            
        }
        
        if(!isBackCamera)
        {
            do {
                let input:AVCaptureDeviceInput = try AVCaptureDeviceInput(device: frontCamera)
                session.addInput(input)
            }
            catch _ {}
        }
        
        imageStillOutput = AVCaptureStillImageOutput()
        let outputSettings:NSDictionary = NSDictionary(object: AVVideoCodecJPEG, forKey: AVVideoCodecKey)
        imageStillOutput.outputSettings = outputSettings as [NSObject : AnyObject]
        session.addOutput(imageStillOutput)
        session.startRunning()
        
    }
    
    func focusCamera(){
        do {
            try backCamera?.lockForConfiguration()
        }
        catch _ {}
        backCamera?.focusMode = AVCaptureFocusMode.AutoFocus
        backCamera?.unlockForConfiguration()
    }
    
    func captureImage(){
        var videoConnection:AVCaptureConnection?
        
        for connection in imageStillOutput.connections as! [AVCaptureConnection]{
            for port in connection.inputPorts as! [AVCaptureInputPort]{
                if port.mediaType == AVMediaTypeVideo{
                    videoConnection = connection
                    videoConnection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                    break
                }
            }
            if videoConnection != nil {
                break
            }
        }
        
        imageStillOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) { (imageSampleBuffer:CMSampleBuffer!, error:NSError!) -> Void in
            
            if imageSampleBuffer != nil {
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageSampleBuffer)
                let image = UIImage(data: imageData)
                self.processImage(image!)
            }
        }
    }
    
    func processImage(image:UIImage){
        self.isThereImage = true
        self.capturedImage.contentMode = UIViewContentMode.ScaleAspectFit
        self.capturedImage.image = image
        self.takenImages.append(image)
        self.reloadViewAfterCapturedImage()
    }
    
    //MARK: - Image Picker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            dismissViewControllerAnimated(true, completion: nil)
            self.takenImages.append(selectedImage)
            self.capturedImage.contentMode = UIViewContentMode.ScaleAspectFit
            self.capturedImage.image = selectedImage
            self.reloadViewAfterCapturedImage()
        }
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Button Actions
    
    @IBAction func switchCameraButtonPressed(sender: UIButton) {
        self.setCurrentPageForCameraScreen()
        if isBackCamera == true {
            isBackCamera = false
            flashButton.hidden = true
            self.initializeCamera()
        }
        else{
            isBackCamera = true
            flashButton.hidden = false
            self.initializeCamera()
        }
    }
    @IBAction func flashButtonPressed(sender: UIButton) {
        self.setCurrentPageForCameraScreen()
        do {
            try backCamera?.lockForConfiguration()
        }
        catch _ {}
        if isFlashActivated == false {
            isFlashActivated = true
            backCamera?.flashMode = AVCaptureFlashMode.On
            self.flashButton.setImage(UIImage(named: "flashOn"), forState: .Normal)
        }
        else {
            isFlashActivated = false
            backCamera?.flashMode = AVCaptureFlashMode.Off
            self.flashButton.setImage(UIImage(named: "flashOff"), forState: .Normal)
        }
        backCamera?.unlockForConfiguration()
    }
    @IBAction func takePhotoButtonPressed(sender: UIButton) {
        self.setCurrentPageForCameraScreen()
        if !isThereImage{
            self.captureImage()
        }
    }
    @IBAction func nextButtonPressed(sender:UIButton){
        self.setCurrentPageForCameraScreen()
        self.isReloadForTheNext = true
        self.prepareForRetake()
        if self.takenImages.count == 2{
            self.creatingPost.leftImage = self.takenImages[0]
            self.creatingPost.rightImage = self.takenImages[1]
            self.capturedImage.image = nil
            self.takenImages.removeAll()
            performSegueWithIdentifier("sendPost", sender: self)
        }

    }
    @IBAction func takeFromGalleryButtonPressed(sender: UIButton) {
        self.setCurrentPageForCameraScreen()
        imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        presentViewController(imagePicker, animated:true, completion:nil)
    }
    @IBAction func retakeButtonPressed(sender: UIButton){
        self.isReloadForTheNext = false
        prepareForRetake()
    }
    
    
    //MARK: - Private Methods
    func prepareVC(){
        
        isFlashActivated = false
        isCapturedPhoto = false
        isBackCamera = true
        isThereImage = false
        isReloadForTheNext = false
        
    }
    
    func reloadViewAfterCapturedImage(){
        isCapturedPhoto = true
        flashButton.hidden = true
        cameraPreview.hidden = true
        takePhotoButton.hidden = true
        switchCameraButton.hidden = true
        takeFromGalleryButton.hidden = true
        
        capturedImage.hidden = false
        retakeButton.hidden = false
        nextButton.hidden = false
        
        if isTheFirstPhoto(){
            nextButton.setTitle("Right Photo >", forState: .Normal)
        }
        else{
            nextButton.setTitle("Post >", forState: .Normal)
        }
    }
    
    func prepareForRetake(){
        if !isReloadForTheNext{
            self.takenImages.removeLast()
        }
        self.capturedImage.image = nil
        self.capturedImage.hidden = true
        self.retakeButton.hidden = true
        self.nextButton.hidden = true

        self.isThereImage = false
        self.isCapturedPhoto = false
        if isBackCamera == true{
            self.flashButton.hidden = false
        }
        self.isReloadForTheNext = false
        self.cameraPreview.hidden = false
        self.takePhotoButton.hidden = false
        self.switchCameraButton.hidden = false
        self.takeFromGalleryButton.hidden = false
    }
    
    func isTheFirstPhoto() -> Bool{
        return !(takenImages.count == 2)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
        if segue.identifier == "sendPost"{
        
            let nav = segue.destinationViewController as! UINavigationController
            let sendPostVC = nav.viewControllers[0] as! SendPostVC
            sendPostVC.creatingPost = self.creatingPost
        }
        
    }
    
}
