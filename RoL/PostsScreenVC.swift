//
//  PostsScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 2.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class PostsScreenVC: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var tempPostHistory = [Post]()
    var convertedPostHistory = [Post]()
    var isConverting:Bool!
    var selectedPost:Post!
    
    @IBOutlet weak var postHistoryCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isConverting = true
        self.view.userInteractionEnabled = false
        self.getUserPostHistory()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "postDeleted", name: "postDeleted", object: nil)
    }
    
    //MARK: - Rol Service
    
    func getUserPostHistory(){
        rolService.getPostHistoryOfUser(UserSession.getInstance().myUser) { (returnedPosts:[Post]?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else{
                self.tempPostHistory = returnedPosts!
                if self.tempPostHistory.count > 0{
                    self.isConverting = true
                    self.convertPostsToUIImage()
                }
                else {
                    self.isConverting = false
                }
            }
        }
    }
    
    func convertPostsToUIImage(){
        let tempPost = tempPostHistory.first
        rolService.convertImageFileToUIImage(tempPost!.leftImageFile) { (returnedImage:UIImage?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else{
                tempPost!.leftImage = returnedImage
                self.rolService.convertImageFileToUIImage(tempPost!.rightImageFile, completionFunction: { (returnedRightImage:UIImage?, error:NSError?) -> Void in
                    if let parseError = error {
                        self.showErrorMessage(parseError)
                    }
                    else{
                        tempPost!.rightImage = returnedRightImage
                        self.convertedPostHistory.append(tempPost!)
                        self.tempPostHistory.removeFirst()
                        if self.tempPostHistory.count > 0 {
                            self.convertPostsToUIImage()
                        }
                        else {
                            // All posts' image files converted to UIImage
                            self.isConverting = false
                            self.activityIndicator.stopAnimating()
                            self.view.userInteractionEnabled = true
                            self.postHistoryCollectionView.reloadData()
                        }
                    }
                })
            }
        }
    }
    
    //MARK: - Collection View Data Source
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isConverting == true{
            return 0
        }
        else {
            if isThereAnyPostOfUser() == false {
                self.showAlertControllerWithTitle("", message: "You don't have any post")
                return 0
            }
            else{
                return self.convertedPostHistory.count
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell:UICollectionViewCell!
        if isThereAnyPostOfUser(){
            cell = configureCellForPostWithIndexPath(indexPath)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.postHistoryCollectionView.deselectItemAtIndexPath(indexPath, animated: true)
        self.selectedPost = self.convertedPostHistory[indexPath.row]
        performSegueWithIdentifier("showPostContent", sender: self)
    }
    
    
    func configureCellForPostWithIndexPath(indexPath:NSIndexPath) -> UICollectionViewCell{
        let cell = postHistoryCollectionView.dequeueReusableCellWithReuseIdentifier("postCell", forIndexPath: indexPath) as! PostCollectionViewCell
        let tempPost = self.convertedPostHistory[indexPath.row]
        
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.layer.cornerRadius = 4
        
        cell.leftImageView.image = tempPost.leftImage
        cell.rightImageView.image = tempPost.rightImage
        cell.leftRate.text = "\(tempPost.leftRate)"
        cell.rightRate.text = "\(tempPost.rightRate)"
        
        return cell
    }
    
    
    //MARK: - Private Methods
    
    func isThereAnyPostOfUser() -> Bool{
        return self.convertedPostHistory.count > 0
    }
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func postDeleted(){
        for var i = 0; i<self.convertedPostHistory.count; i++ {
            let tempPost = self.convertedPostHistory[i]
            if tempPost.parseObject.objectId == selectedPost.parseObject.objectId{
                self.convertedPostHistory.removeAtIndex(i)
                self.postHistoryCollectionView.reloadData()
            }
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPostContent"{
            let rateVC = segue.destinationViewController as! RatePostScreenVC
            rateVC.selectedPost = self.selectedPost
            rateVC.isPostFromProfile = true
        }
        
    }
}
