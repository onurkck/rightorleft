//
//  ScrollerVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class ScrollerVC: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    
    var containerView1:UIView!
    var containerView2:UIView!
    var containerView3:UIView!
    
    var inboxScreenVC:InboxScreenVC = InboxScreenVC()
    var cameraScreenVC:CameraScreenVC! = CameraScreenVC()
    var profileScreenVC:ProfileScreenVC = ProfileScreenVC()
    
    var currentPage:Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.addSubview(inboxScreenVC.view)
        self.scrollView.addSubview(cameraScreenVC.view)
        self.scrollView.addSubview(profileScreenVC.view)
        
        setupContainers()
        setupViewControllers()
       
     
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        
        if UserSession.getInstance().currentPage == nil {
            UserSession.getInstance().currentPage = 1
        }
        
        self.currentPage = UserSession.getInstance().currentPage
        self.setCurrentPage(self.currentPage, animated: false)

    }
    
    func setupContainers()
    {
        containerView1 = UIView(frame: CGRectZero)
        containerView2 = UIView(frame: CGRectZero)
        containerView3 = UIView(frame: CGRectZero)
        
        
        scrollView.addSubview(containerView1)
        scrollView.addSubview(containerView2)
        scrollView.addSubview(containerView3)
        
        containerView1.translatesAutoresizingMaskIntoConstraints = false
        
        
        let widthContraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView1,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView1,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0, constant: 0)
        
        let leftConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView1,
            attribute: NSLayoutAttribute.Leading,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView1,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        scrollView.addConstraints([widthContraint, heightContraint, leftConstraint, topConstraint])
        
        
        containerView2.translatesAutoresizingMaskIntoConstraints = false
        
        let widthContraint2:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView2,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint2:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView2,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 0)
        
        let leftConstraint2:NSLayoutConstraint = NSLayoutConstraint(item: containerView2,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView1,
            attribute: NSLayoutAttribute.Trailing,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint2:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView2,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        scrollView.addConstraints([widthContraint2, heightContraint2, leftConstraint2, topConstraint2])
        
        
        
        containerView3.translatesAutoresizingMaskIntoConstraints = false
        
        let widthContraint3:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView3,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint3:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView3,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 0)
        
        let leftConstraint3:NSLayoutConstraint = NSLayoutConstraint(item: containerView3,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView2,
            attribute: NSLayoutAttribute.Trailing,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint3:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView3,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        let rightConstraint:NSLayoutConstraint = NSLayoutConstraint(item: scrollView,
            attribute: NSLayoutAttribute.Trailing,
            relatedBy: NSLayoutRelation.Equal,
            toItem: containerView3,
            attribute: NSLayoutAttribute.Trailing,
            multiplier: 1.0,
            constant: 0)
        
        scrollView.addConstraints([widthContraint3, heightContraint3, leftConstraint3, topConstraint3, rightConstraint])
    }
    
    func setupViewControllers()
    {
        inboxScreenVC = storyboard?.instantiateViewControllerWithIdentifier("InboxVC") as! InboxScreenVC
        addChildViewController(inboxScreenVC)
        containerView1.addSubview(inboxScreenVC.view)
        
        let widthContraint:NSLayoutConstraint = NSLayoutConstraint(item: containerView1,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: inboxScreenVC.view,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint:NSLayoutConstraint = NSLayoutConstraint(item: containerView1,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: inboxScreenVC.view,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 0)
        
        let leftConstraint:NSLayoutConstraint = NSLayoutConstraint(item: containerView1,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: inboxScreenVC.view,
            attribute: NSLayoutAttribute.Leading,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint:NSLayoutConstraint = NSLayoutConstraint(item: containerView1,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: inboxScreenVC.view,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        inboxScreenVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView1.addConstraints([widthContraint, heightContraint, leftConstraint, topConstraint])
        
        
        
        cameraScreenVC = storyboard?.instantiateViewControllerWithIdentifier("CameraVC") as! CameraScreenVC
        addChildViewController(cameraScreenVC)
        containerView2.addSubview(cameraScreenVC.view)
        
        let widthContraint2:NSLayoutConstraint = NSLayoutConstraint(item: containerView2,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: cameraScreenVC.view,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint2:NSLayoutConstraint = NSLayoutConstraint(item: containerView2,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: cameraScreenVC.view,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 0)
        
        let leftConstraint2:NSLayoutConstraint = NSLayoutConstraint(item: containerView2,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: cameraScreenVC.view,
            attribute: NSLayoutAttribute.Leading,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint2:NSLayoutConstraint = NSLayoutConstraint(item: containerView2,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: cameraScreenVC.view,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        cameraScreenVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView2.addConstraints([widthContraint2, heightContraint2, leftConstraint2, topConstraint2])
        
        
        profileScreenVC = storyboard?.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileScreenVC
        addChildViewController(profileScreenVC)
        containerView3.addSubview(profileScreenVC.view)
        
        let widthContraint3:NSLayoutConstraint = NSLayoutConstraint(item: containerView3,
            attribute: NSLayoutAttribute.Width,
            relatedBy: NSLayoutRelation.Equal,
            toItem: profileScreenVC.view,
            attribute: NSLayoutAttribute.Width,
            multiplier: 1.0,
            constant: 0)
        
        let heightContraint3:NSLayoutConstraint = NSLayoutConstraint(item: containerView3,
            attribute: NSLayoutAttribute.Height,
            relatedBy: NSLayoutRelation.Equal,
            toItem: profileScreenVC.view,
            attribute: NSLayoutAttribute.Height,
            multiplier: 1.0,
            constant: 0)
        
        let leftConstraint3:NSLayoutConstraint = NSLayoutConstraint(item: containerView3,
            attribute: NSLayoutAttribute.Leading,
            relatedBy: NSLayoutRelation.Equal,
            toItem: profileScreenVC.view,
            attribute: NSLayoutAttribute.Leading,
            multiplier: 1.0,
            constant: 0)
        
        let topConstraint3:NSLayoutConstraint = NSLayoutConstraint(item: containerView3,
            attribute: NSLayoutAttribute.Top,
            relatedBy: NSLayoutRelation.Equal,
            toItem: profileScreenVC.view,
            attribute: NSLayoutAttribute.Top,
            multiplier: 1.0,
            constant: 0)
        
        profileScreenVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView3.addConstraints([widthContraint3, heightContraint3, leftConstraint3, topConstraint3])
    }
    
    func setCurrentPage(page:Int, animated:Bool)
    {
        let scrollWidth:CGPoint = CGPointMake((CGFloat)((Int)(self.scrollView.frame.size.width) * page), 0.0)
        self.scrollView.setContentOffset(scrollWidth, animated: animated)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.currentPage = (Int)((scrollView.contentOffset.x) / (scrollView.frame.size.width))
        switch self.currentPage {
        case 0:
            NSNotificationCenter.defaultCenter().postNotificationName("inboxScreenWillBeShown", object: nil)
            break
        case 1:
            print("Camera screen will be shown")
            break
        case 2:
            print("Profile screen will be shown")
            break
        default:
            break
        }
    }
}
