//
//  InboxScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class InboxScreenVC: BaseViewController, UITableViewDelegate,UITableViewDataSource,RatingProtocol {
    @IBOutlet weak var inboxTableView: UITableView!
    var incomingPosts:[Post]!
    var refreshControl:UIRefreshControl!
    private var selectedPost:Post!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareVC()
    }
    
    override func viewWillAppear(animated: Bool) {
        if self.incomingPosts.count>0{
            self.inboxTableView.reloadData()
        }
    }
    
    // MARK: - Parse Services
    func getPosts(){
        self.rolService.getPostsForAUser(currentUser!) { (postArray:[Post]?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else {
                self.incomingPosts = postArray
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Inbox"
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.incomingPosts.count == 0{
            return 1
        }
        return self.incomingPosts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        if self.incomingPosts.count == 0 {
            return tableView.dequeueReusableCellWithIdentifier("noPostCell", forIndexPath: indexPath)
        }
    
        let tempPost = incomingPosts[indexPath.row] as Post
        cell = configureCellWithSenderUserName(tempPost.senderUser.userName, indexPath: indexPath)
    
        return cell
    }
    
    func configureCellWithSenderUserName(senderUserName:String, indexPath:NSIndexPath) -> UITableViewCell{
        let cell = self.inboxTableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        let titleLabel = cell.viewWithTag(1) as! UILabel
        titleLabel.text = senderUserName
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedPost = self.incomingPosts[indexPath.row]
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("showPostDetail", sender: self)
    }
    
    //MARK: - Private Methods
    func inboxScreenWillBeShown(){
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: "tableViewPulled", forControlEvents: .ValueChanged)
            inboxTableView.addSubview(refreshControl)
        }
        self.inboxTableView.reloadData()
        self.setCurrentPageForInboxScreen()
    }
    
    func prepareVC(){
        incomingPosts = [Post]()
        self.getPosts()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "inboxScreenWillBeShown", name: "inboxScreenWillBeShown", object: nil)
    }
    
    func tableViewPulled(){
        self.getPosts()
        self.inboxTableView.reloadData()
        self.refreshControl.endRefreshing()
    }

    func postWasRated(post: Post) {
        for var i = 0; i<self.incomingPosts.count; i++ {
          let tempPost = self.incomingPosts[i]
            if tempPost.parseObject.objectId == post.parseObject.objectId{
                self.incomingPosts.removeAtIndex(i)
                self.inboxTableView.reloadData()
                break
            }
        }
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPostDetail"{
            let rateNavVC = segue.destinationViewController as! UINavigationController
            let rateVC = rateNavVC.viewControllers[0] as! RatePostScreenVC
            rateVC.selectedPost = self.selectedPost
            rateVC.isPostFromProfile = false
            rateVC.ratingDelegate = self
            
        }
    }

}
