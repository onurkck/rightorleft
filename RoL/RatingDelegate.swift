//
//  RatingDelegate.swift
//  RoL
//
//  Created by Onur Küçük on 5.12.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import Foundation
import UIKit
protocol RatingProtocol {
    func postWasRated(post:Post)
}