//
//  LoginVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class LoginVC: BaseViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var loginTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var userNameTextField:UITextField!
    var passwordTextField:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    //MARK: - Parse Service Methods
    
    func loginWithUserNameAndPassword(userName:String, password:String){
        rolService.loginWithUserName(userName, password: password) { (returnedUser:User?, error:NSError?) -> Void in
            self.activityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                self.goToMainScreen()
                
            }
        }
    }
    
    //MARK: - Table View Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        switch indexPath.row {
        case 0:
            cell = loginTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            userNameTextField = cell.viewWithTag(1) as! UITextField
            userNameTextField.textAlignment = .Center
            userNameTextField.placeholder = "Username"
            userNameTextField.delegate = self
            textFieldShouldReturn(userNameTextField)
        case 1:
            cell = loginTableView.dequeueReusableCellWithIdentifier("CellWithTextField", forIndexPath: indexPath)
            passwordTextField = cell.viewWithTag(1) as! UITextField
            passwordTextField.textAlignment = .Center
            passwordTextField.placeholder = "Password"
            passwordTextField.secureTextEntry = true
            passwordTextField.delegate = self
            textFieldShouldReturn(passwordTextField)
        case 2:
            cell = loginTableView.dequeueReusableCellWithIdentifier("CellWithButton", forIndexPath: indexPath)
            let loginButton = cell.viewWithTag(1) as! UIButton
            loginButton.setTitle("Login", forState: .Normal)
            loginButton.addTarget(self, action: "login:", forControlEvents: .TouchUpInside)
        case 3:
            cell = loginTableView.dequeueReusableCellWithIdentifier("CellWithButton", forIndexPath: indexPath)
            let signUpButton = cell.viewWithTag(1) as! UIButton
            signUpButton.setTitle("Sign Up", forState: .Normal)
            signUpButton.addTarget(self, action: "signUp:", forControlEvents: .TouchUpInside)
        default:
            return loginTableView.dequeueReusableCellWithIdentifier("CellWithButton", forIndexPath: indexPath)
        }
        return cell
    }
    
    
    //MARK: - Cell Button Actions
    func login(sender:UIButton){
        let userName = userNameTextField.text
        let password = passwordTextField.text
        if (userName != "" && password != ""){
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            self.loginWithUserNameAndPassword(userName!, password: password!)
        }
        else{
            self.showAlertControllerWithTitle("Error", message: "Username and password couldn't be blank.")
        }
    }
    
    func signUp(sender:UIButton){
        self.performSegueWithIdentifier("showSignUp", sender: self)
    }
    
}
