//
//  ParseRoLServices.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import Foundation
import UIKit
import Parse

let kClassNameUser = "_User"
let kClassNameUserFriends = "UserFriends"
let kClassNameFriendRequest = "FriendRequest"
let kClassNamePost = "Post"
let kClassNameGroup = "Group"

let kAcceptFriendReq = "Accept"
let kDeclineFriendReq = "Decline"
let kWaitingFriendReq = "Waiting"

let kSideLeftRate = "leftRate"
let kSideRightRate = "rightRate"

class ParseRoLServices:RoLServices {
    //MARK: - Login Functions
    
    func loginWithUserName(userName:String, password:String, completionFunction:UserReturnFunction){
        PFUser.logInWithUsernameInBackground(userName, password: password) { (returnedUser:PFUser?, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else{
                if returnedUser == nil{
                    completionFunction(nil,NSError(domain: "DLParse", code: 0, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Login failed please try again.", comment: "Login fail")]))
                }
                else{
                    let loggedUser = User.initWithParseObject(returnedUser!) as User
                    UserSession.getInstance().myUser = loggedUser
                    self.saveCurrentUserDevice()
                    completionFunction(loggedUser,nil)
                }
            }
        }
    }
    
    func signUpUser(userName:String, email:String, password:String, completionFunction:UserReturnFunction){
        let pfUserObj = PFUser()
        pfUserObj.username = userName
        pfUserObj.email = email
        pfUserObj.password = password
        pfUserObj["device"] = PFInstallation.currentInstallation()
        let userObj = User()
        userObj.userName = userName
        userObj.userEmail = email
        userObj.deviceObject = PFInstallation.currentInstallation()
        userObj.parseObject = pfUserObj
        
        self.doesUserEmailExist(userObj) { (response:String?, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else if (response != "" && response != nil){
                completionFunction(nil,NSError(domain: "DLParse", code: 0, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString(response!, comment: "username or user email exist")]))
            }
            return
        }
        pfUserObj.signUpInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else{
                if success == true {
                    UserSession.getInstance().myUser = userObj
                    completionFunction(userObj,nil)
                }
                else{
                    completionFunction(nil,NSError(domain: "DLParse", code: 0, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Sign up process failed please try again.", comment: "sign up was fail")]))
                }
            }
        }
    }
    
    func logoutUser(user:User, completionFunction:BooleanReturnFunction){
        PFUser.logOutInBackgroundWithBlock { (error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(false,parseError)
            }
            else{
                completionFunction(true,nil)
            }
        }
    }
    
    func changePassword(user:User, completionFunction:BooleanReturnFunction){
        PFUser.requestPasswordResetForEmailInBackground(user.userEmail) { (success:Bool, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else {
                completionFunction(success,nil)
            }
        }
    }
    
    //MARK: - Post Related Functions
    
    func getPostsForAUser(recieverUser:User, completionFunction:MultiplePostReturnFunction){
        let query = PFQuery(className: kClassNameGroup)
        var returnedPosts = [Post]()
        query.includeKey("recieverUser.User")
        query.includeKey("post.Post")
        query.includeKey("post.senderUser.User")
        query.whereKey("recieverUser", equalTo: recieverUser.parseObject)
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackgroundWithBlock { (returnedPostIDs:[PFObject]?, error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(nil,parseError)
            }
            else{
                if returnedPostIDs?.count == 0 {
                    completionFunction(returnedPosts, nil)
                }
                for returnedObj in returnedPostIDs! {
                    let tempPost = Post.initWithParseObject(returnedObj.objectForKey("post") as! PFObject) as Post
                    returnedPosts.append(tempPost)
                    if returnedPosts.count == returnedPostIDs?.count{
                        completionFunction(returnedPosts,nil)
                    }
                }
            }
        }
    }
    
    func getPostHistoryOfUser(currentUser:User, completionFunction:MultiplePostReturnFunction){
        let query = PFQuery(className: kClassNamePost)
        var returnedPosts = [Post]()
        query.whereKey("senderUser", equalTo: currentUser.parseObject)
        query.includeKey("senderUser.User")
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackgroundWithBlock { (returnedPostIDs:[PFObject]?, error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(nil,parseError)
            }
            else{
                if returnedPostIDs?.count == 0 {
                    completionFunction(returnedPosts, nil)
                }
                for returnedObj in returnedPostIDs! {
                    let tempPost = Post.initWithParseObject(returnedObj) as Post
                    returnedPosts.append(tempPost)
                    if returnedPosts.count == returnedPostIDs?.count{
                        completionFunction(returnedPosts,nil)
                    }
                }
            }
        }
    }
    
    func sendPostToUserList(post:Post, userList:[User], completionFunction:BooleanReturnFunction){
        let newPostObject = PFObject(className: kClassNamePost)
        
        let leftImageData = UIImageJPEGRepresentation(post.leftImage, 0.25)
        let rightImageData = UIImageJPEGRepresentation(post.rightImage, 0.25)
        
        let leftImageFile = PFFile(name: "leftImage.png", data: leftImageData!)
        let rightImageFile = PFFile(name: "rightImage.png", data: rightImageData!)
        
        newPostObject.setObject(UserSession.getInstance().myUser.parseObject, forKey: "senderUser")
        newPostObject.setObject(leftImageFile, forKey: "leftImage")
        newPostObject.setObject(rightImageFile, forKey: "rightImage")
        newPostObject.setObject(0, forKey: "leftRate")
        newPostObject.setObject(0, forKey: "rightRate")
        newPostObject.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(nil,parseError)
            }
            else{
                post.parseObject = newPostObject
                self.createGroupForPost(post, userGroup: userList, completionFunction: { (success:Bool?, error:NSError?) -> Void in
                    if let parserError = error {
                        completionFunction(false, parserError)
                    }
                    else{
                        completionFunction(success,nil)
                    }
                })
            }
        }
        
    }
    
    func ratePostWithSide(ratingPost:Post, side:String, newRate:Int, completionFunction:BooleanReturnFunction){
        ratingPost.parseObject.setObject(newRate, forKey: side)
        ratingPost.parseObject.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(false, parseError)
            }
            else{
                self.deleteRatedPostFromInbox(ratingPost)
                completionFunction(success,nil)
            }
        }
    }
    
    func deletePost(deletingPost:Post, completionFunction:BooleanReturnFunction){
        let query = PFQuery(className: kClassNameGroup)
        query.whereKey("post", equalTo: deletingPost.parseObject)
        query.findObjectsInBackgroundWithBlock { (returnedObjects:[PFObject]?, error:NSError?) -> Void in
            if returnedObjects != nil {
                PFObject.deleteAllInBackground(returnedObjects, block: { (success:Bool, error:NSError?) -> Void in
                    deletingPost.parseObject.deleteInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                        if let parseError = error {
                            completionFunction(false,parseError)
                        }
                        else{
                            completionFunction(success,nil)
                        }
                    }
                })
            }
        }
        
    }
    
    func convertImageFileToUIImage(imageFile:PFFile, completionFunction:ImageReturnFunction){
        
        var convertedImage:UIImage?
        imageFile.getDataInBackgroundWithBlock({ (imageData:NSData?, error:NSError?) -> Void in
            if error != nil {
                completionFunction(nil, error)
            }
            else {
                if imageData == nil {
                    completionFunction(nil,NSError(domain: "DLParse", code: 0, userInfo: [NSLocalizedDescriptionKey:NSLocalizedString("Images couldn't fetched", comment: "Image data is nil.")]))
                }
                else {
                    convertedImage = UIImage(data: imageData!)
                    completionFunction(convertedImage,nil)
                }
                
            }
            }) { (Int32) -> Void in
        }
    }
    
    
    //MARK: - Friends Related Functions
    
    func sendFriendRequesToUser(senderUser:User, recieverUserName:String, completionFunction:BooleanReturnFunction){
        
        let findUserQuery = PFQuery(className: kClassNameUser)
        findUserQuery.whereKey("username", equalTo: recieverUserName)
        findUserQuery.getFirstObjectInBackgroundWithBlock { (returnedUser:PFObject?, error:NSError?) -> Void in
            if let parseError = error {
                completionFunction(false, parseError)
            }
            else {
                let recieverUser = User.initWithParseObject(returnedUser as! PFUser)
                
                self.isFriendRequestAlreadySent(senderUser, recieverUser: recieverUser) { (isSent:Bool?, error:NSError?) -> Void in
                    if let parseError = error {
                        completionFunction(nil,parseError)
                    }
                    else {
                        if isSent == false{
                            let friendRequestObject = PFObject(className: kClassNameFriendRequest)
                            friendRequestObject.setValue(senderUser.parseObject, forKeyPath: "senderUser")
                            friendRequestObject.setValue(recieverUser.parseObject, forKeyPath: "recieverUser")
                            friendRequestObject.setObject(kWaitingFriendReq, forKey: "recieverResponse")
                            
                            friendRequestObject.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                if let parseError = error {
                                    completionFunction(false, parseError)
                                }
                                else {
                                    self.sendPushNotificationToUserForFriendReq(recieverUser, completionFunction: { (success:Bool?, error:NSError?) -> Void in
                                        
                                        
                                    })
                                    completionFunction(success,nil)
                                }
                            })
                        }
                        else {
                            completionFunction(false, NSError(domain: "DLParse", code: 0, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("You sent the friend request to \(recieverUser.userName) already.", comment: "")]))
                        }
                    }
                }
            }
        }
    }
    
    func checkingFriendRequest(recieverUser:User, completionFunction:FriendRequestsReturnFunction){
        var friendRequests = [FriendRequest]()
        let query = PFQuery(className: kClassNameFriendRequest)
        query.includeKey("senderUser.User")
        query.includeKey("recieverUser.User")
        query.whereKey("recieverUser", equalTo: recieverUser.parseObject)
        query.whereKey("recieverResponse", equalTo: kWaitingFriendReq)
        query.findObjectsInBackgroundWithBlock { (returnedObjects:[PFObject]?, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else {
                if returnedObjects?.count == 0{
                    completionFunction(friendRequests,nil)
                }
                else{
                    for friendReq in returnedObjects!{
                        let tempFriendReq = FriendRequest.initWithParseObject(friendReq) as FriendRequest
                        friendRequests.append(tempFriendReq)
                        if friendRequests.count == returnedObjects?.count{
                            completionFunction(friendRequests,nil)
                        }
                    }
                }
            }
        }
    }
    
    func responseFriendRequest(friendRequest:FriendRequest, response:String, completionFunction:BooleanReturnFunction){
        if response == kAcceptFriendReq{
            friendRequest.parseObject.setObject(response, forKey: "recieverResponse")
            friendRequest.parseObject.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                if let parseError = error {
                    completionFunction(nil, parseError)
                }
                else{
                    self.acceptedFriendRequest(friendRequest.senderUser, recieverUser: friendRequest.recieverUser, completionFunction: { (succeed:Bool?, error:NSError?) -> Void in
                        if let parseError = error {
                            completionFunction(nil,parseError)
                        }
                        else {
                            completionFunction(succeed,nil)
                        }
                    })
                }
            })
        }
        
    }
    
    func getUserAllFriends(myUser:User, completionFunction:MultipleUserReturnFunction){
        var userFriends = [User]()
        let query = PFQuery(className: kClassNameUserFriends)
        query.whereKey("user1", equalTo: myUser.parseObject)
        query.includeKey("user2.User")
        query.includeKey("user2.device.Installation")
        query.findObjectsInBackgroundWithBlock { (returnedFriends:[PFObject]?, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else{
                if returnedFriends?.count == 0 {
                    completionFunction(userFriends,nil)
                }
                for returnedUserObj in returnedFriends!{
                    let tempUser = User.initWithParseObject(returnedUserObj["user2"] as! PFUser) as User
                    userFriends.append(tempUser)
                    if userFriends.count == returnedFriends?.count{
                        UserSession.getInstance().myUserFriendLists = userFriends
                        completionFunction(userFriends,nil)
                    }
                }
            }
        }
    }
    
    //MARK: - Helper Functions
    
    func saveCurrentUserDevice(){
        let currentInstallation = PFInstallation.currentInstallation()
        if currentInstallation != PFUser.currentUser()!["device"] as! PFInstallation{
            PFUser.currentUser()!["device"] = currentInstallation
            PFUser.currentUser()?.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                print("device saving status : \(success)")
            })
        }
    }
    func sendPushNotificationToUserForFriendReq(toUser:User, completionFunction:BooleanReturnFunction) {
        let pushQuery = PFInstallation.query()
        let device: PFInstallation? = toUser.deviceObject
        pushQuery?.whereKey("objectId", equalTo: (device?.objectId)!)
        
        let currentUsername:String = PFUser.currentUser()!.username! as String
        let push = PFPush()
        
        push.setQuery(pushQuery)
        push.setMessage("\(currentUsername) send you friend request.")
        push.sendPushInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
        }
    }
    
    func sendPushNotificationsForPost(toUserList:[User],completionFunction:BooleanReturnFunction){
        
    }
    
    func doesUserEmailExist(user: User, completionFunction: ResponseReturnFunction) {
        let emailQuery = PFQuery(className: kClassNameUser)
        emailQuery.whereKey("email", equalTo: user.userEmail)
        
        emailQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
            if let parseError = error {
                completionFunction(nil,parseError)
            }
            if results?.count > 0 {
                completionFunction(NSLocalizedString("User email has already exist.", comment: "user email exists"),nil)
            }
            else {
                self.doesUserNameExist(user, completionFunction: { (response:String?, userNameError:NSError?) -> Void in
                    if userNameError != nil{
                        completionFunction(nil,userNameError)
                    }
                    else{
                        completionFunction(response,nil)
                    }
                })
            }
        }
    }
    
    func doesUserNameExist(user: User, completionFunction: ResponseReturnFunction) {
        let emailQuery = PFQuery(className: kClassNameUser)
        emailQuery.whereKey("username", equalTo: user.userName)
        
        emailQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
            if let parseError = error {
                completionFunction(nil,parseError)
            }
            if results?.count > 0 {
                completionFunction(NSLocalizedString("Username has alredy taken. Please choose another username", comment: "username exists"),nil)
            }
            else {
                completionFunction(nil,nil)
            }
        }
    }
    
    func isFriendRequestAlreadySent(senderUser:User, recieverUser:User, completionFunction:BooleanReturnFunction){
        let query = PFQuery(className: kClassNameFriendRequest)
        
        query.whereKey("senderUser", equalTo: senderUser.parseObject)
        query.whereKey("recieverUser", equalTo: recieverUser.parseObject)
        
        query.findObjectsInBackgroundWithBlock { (returnedObj:[PFObject]?, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else {
                if returnedObj?.count == 0{
                    completionFunction(false,nil)
                }
                else {
                    completionFunction(true,nil)
                }
            }
        }
    }
    
    func acceptedFriendRequest(senderUser:User, recieverUser:User, completionFunction:BooleanReturnFunction){
        let acceptedFriendObj = PFObject(className: kClassNameUserFriends)
        acceptedFriendObj.setValue(senderUser.parseObject, forKeyPath: "user1")
        acceptedFriendObj.setValue(recieverUser.parseObject, forKeyPath: "user2")
        acceptedFriendObj.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if let parserError = error {
                completionFunction(nil,parserError)
            }
            else {
                let acceptedFriendObj2 = PFObject(className: kClassNameUserFriends)
                acceptedFriendObj2.setValue(recieverUser.parseObject, forKeyPath: "user1")
                acceptedFriendObj2.setValue(senderUser.parseObject, forKeyPath: "user2")
                acceptedFriendObj2.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    
                    if let parseError = error {
                        completionFunction(nil,parseError)
                    }
                    else {
                        completionFunction(success,nil)
                    }
                })
            }
        }
    }
    
    func createGroupForPost(post:Post, userGroup:[User], completionFunction:BooleanReturnFunction){
        let newGroupObject = PFObject(className: kClassNameGroup)
        newGroupObject.setObject(post.parseObject, forKey: "post")
        newGroupObject.setObject(userGroup[0].parseObject, forKey: "recieverUser")
        newGroupObject.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
            if let parseError = error{
                completionFunction(nil,parseError)
            }
            else{
                if success{
                    var newUserArray = userGroup
                    newUserArray.removeFirst()
                    if newUserArray.count > 0{
                        self.createGroupForPost(post, userGroup: newUserArray, completionFunction: { (success:Bool?, error:NSError?) -> Void in
                        })
                    }
                    else {
                        completionFunction(success,nil)
                    }
                }
                else{
                    completionFunction(false,nil)
                }
            }
        })
    }
    
    func deleteRatedPostFromInbox(ratedPost:Post){
        let deleteQuery = PFQuery(className: kClassNameGroup)
        deleteQuery.whereKey("post", equalTo: ratedPost.parseObject)
        deleteQuery.whereKey("recieverUser", equalTo: UserSession.getInstance().myUser.parseObject)
        
        deleteQuery.getFirstObjectInBackgroundWithBlock { (returnedObject:PFObject?, error:NSError?) -> Void in
            if error != nil {
                print("Couldn't deleted due to\(error?.description)")
            }
            else{
                returnedObject?.deleteInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                    if success{
                        print("Successfully deleted.")
                        NSNotificationCenter.defaultCenter().postNotificationName("inboxScreenWillBeShown", object: nil)
                    }
                })
            }
        }
    }
}