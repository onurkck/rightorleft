//
//  PageContentVC.swift
//  RoL
//
//  Created by Onur Küçük on 30.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class PageContentVC: BaseViewController {
    
    var pageIndex:Int!
    var image:UIImage!
    var selectedPost:Post!
    var myProtocol:RatingProtocol?
    var isPostFromProfile:Bool!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var rateResultLabel: UILabel!
    @IBOutlet weak var imageRate: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = image
        if isPostFromProfile == true {
            self.imageRate.hidden = false
            self.topConstraint.constant = 0
            if pageIndex == 0 {
                self.imageRate.text = "\(self.selectedPost.leftRate)"
            }
            else {
                self.imageRate.text = "\(self.selectedPost.rightRate)"
            }
        }
        else{
            self.topConstraint.constant = 60
        }
        self.activityIndicator.stopAnimating()
    }
    
    //MARK: - Rol Services
    func ratePostWithSide(side:String, newRate:Int){
        rolService.ratePostWithSide(selectedPost, side: side, newRate: newRate) { (success:Bool?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else {
                self.animateRateResultLabel()
            }
        }
    }
    
    @IBAction func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func doubleTapped(sender: UITapGestureRecognizer) {
        if !isPostFromProfile{
        print("Double tapped.")
        var side = String()
        var newRate = Int()
        if pageIndex == 0{
            side = kSideLeftRate
            newRate = selectedPost.leftRate + 1
            self.selectedPost.leftRate = newRate
        }
        else{
            side = kSideRightRate
            newRate = selectedPost.rightRate + 1
            self.selectedPost.rightRate = newRate
        }
        self.ratePostWithSide(side, newRate: newRate)
        }
    }
    
    func animateRateResultLabel(){
        self.rateResultLabel.hidden = false
        UIView.animateWithDuration(1, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.rateResultLabel.text = "\(self.selectedPost.leftRate) / \(self.selectedPost.rightRate)"
            self.rateResultLabel.alpha = 0
            }) { (success) -> Void in
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    self.myProtocol?.postWasRated(self.selectedPost)
                })
        }
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
