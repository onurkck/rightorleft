//
//  FriendRequest.swift
//  RoL
//
//  Created by Onur Küçük on 19.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit
import Parse

class FriendRequest: NSObject {
    
    var senderUser:User!
    var recieverUser:User!
    var status:String?
    var parseObject:PFObject!
    
    static func initWithParseObject(parseObject:PFObject) -> FriendRequest {
        let friendRequest = FriendRequest()
        friendRequest.senderUser = User.initWithParseObject(parseObject.objectForKey("senderUser") as! PFUser)
        friendRequest.recieverUser = User.initWithParseObject(parseObject.objectForKey("recieverUser") as! PFUser)
        friendRequest.status = parseObject.objectForKey("recieverResponse") as? String
        friendRequest.parseObject = parseObject
        return friendRequest
    }
    func convertToParseObject(friendRequestObj:FriendRequest) -> PFObject {
        friendRequestObj.parseObject.setValue(User.convertToParseObject(senderUser), forKeyPath: "senderUser")
        friendRequestObj.parseObject.setValue(User.convertToParseObject(recieverUser), forKeyPath: "recieverUser")
        friendRequestObj.parseObject.setValue(status, forKeyPath: "status")
        return friendRequestObj.parseObject
    }
    
}
