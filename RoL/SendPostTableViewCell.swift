//
//  SendPostTableViewCell.swift
//  RoL
//
//  Created by Onur Küçük on 9.12.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class SendPostTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    var isChecked:Bool!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
