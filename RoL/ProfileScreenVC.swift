//
//  ProfileScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 31.10.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class ProfileScreenVC: BaseViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: -Parse Methods
    
    func changePassword(user:User){
        rolService.changePassword(user) { (success:Bool?, error:NSError?) -> Void in
            if let parseError = error{
                self.showErrorMessage(parseError)
            }
            else{
                if success == true{
                    self.showAlertControllerWithTitle("Success", message: "Reset password request was sent to \(user.userEmail). Please check your mails.")
                }
                else {
                    self.showAlertControllerWithTitle("Warning", message: "There was an error occured, please try again.")
                }
            }
        }
    }
    
    func logoutUser(user:User){
        rolService.logoutUser(user) { (success:Bool?, error:NSError?) -> Void in
            self.setCurrentPageForCameraScreen()
            self.activityIndicator.stopAnimating()
            UIApplication.sharedApplication().endIgnoringInteractionEvents()
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else {
                if success == true {
                    self.showLogoutSuccess()
                }
                else {
                    self.showAlertControllerWithTitle("Warning", message: "There was an error occured while loging out, plese try again.")
                }
            }
        }
    }
    
    //MARK: - Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        else {
            return 4
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "USER"
        }
        else{
            return "SETTINGS"
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            return configureUserNameCellWithUserName(indexPath)
        }
        else{
            switch indexPath.row {
            case 0:
                return configureEntityCell("Friends", indexPath: indexPath)
            case 1:
                return configureEntityCell("Posts", indexPath: indexPath)
            case 2:
                return configureEntityCell("Change Password", indexPath: indexPath)
            case 3:
                return configureEntityCell("Logout", indexPath: indexPath)
            default:
                return configureEntityCell("", indexPath: indexPath)
            }
        }
    }
    
    //MARK: Cell Methods
    func configureUserNameCellWithUserName(indexPath:NSIndexPath) -> UITableViewCell{
        let cell = profileTableView.dequeueReusableCellWithIdentifier("UserNameCell", forIndexPath: indexPath)
        let userNameLabel = cell.viewWithTag(1) as! UILabel
        userNameLabel.text = UserSession.getInstance().myUser.userName
        
        return cell
    }
    
    func configureEntityCell(entityName:String, indexPath:NSIndexPath) -> UITableViewCell{
        let cell = profileTableView.dequeueReusableCellWithIdentifier("EntityCell", forIndexPath: indexPath)
        let cellButton = cell.viewWithTag(1) as! UIButton
        cellButton.setTitle(entityName, forState: .Normal)
        cellButton.tag = indexPath.row
        return cell
    }
    @IBAction func cellButtonAction(sender: UIButton) {
        
        self.setCurrentPageForProfileScreen()
        
        switch sender.tag{
        case 0:
            performSegueWithIdentifier("showUserFriends", sender: self)
        case 1:
            performSegueWithIdentifier("showUserPosts", sender: self)
        case 2:
            self.showChangePasswordAlert()
        case 3:
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            self.logoutUser(UserSession.getInstance().myUser)
        default:
            break
        }
    }
    
    //MARK: - Private Methods
    func showLogoutSuccess(){
        let alertcontroller:UIAlertController = UIAlertController(title: NSLocalizedString("Success", comment: "title"), message:NSLocalizedString("Logged out successfully", comment: "message"), preferredStyle: UIAlertControllerStyle.Alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (loggedOut) -> Void in
            UserSession.getInstance().removeSession()
            let loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginFlowNav") as! UINavigationController
            self.presentViewController(loginVC, animated: true, completion: nil)
        }))
        self.presentViewController(alertcontroller, animated: true, completion: nil)
    }
    
    func showChangePasswordAlert(){
        let alertcontroller:UIAlertController = UIAlertController(title: NSLocalizedString("Warning", comment: "title"), message:NSLocalizedString("Password reset mail will be sent to your email address. Are you sure?", comment: "message"), preferredStyle: UIAlertControllerStyle.Alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (loggedOut) -> Void in
            self.changePassword(UserSession.getInstance().myUser)
        }))
        alertcontroller.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertcontroller, animated: true, completion: nil)
    }
}
