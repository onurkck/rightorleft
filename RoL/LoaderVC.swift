//
//  LoaderVC.swift
//  RoL
//
//  Created by Onur Küçük on 2.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class LoaderVC: UIViewController {
    @IBOutlet weak var titileLabel: UILabel!
    var titleText:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.titileLabel.text = titleText
    }
}
