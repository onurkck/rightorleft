//
//  RatePostScreenVC.swift
//  RoL
//
//  Created by Onur Küçük on 30.11.2015.
//  Copyright © 2015 Onur Küçük. All rights reserved.
//

import UIKit

class RatePostScreenVC: BaseViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {
    
    var imagesArray:[UIImage]!
    var selectedPost:Post!
    var pageViewController:UIPageViewController!
    var ratingDelegate:RatingProtocol?
    var isPostFromProfile:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagesArray = [UIImage]()
        if !isPostFromProfile{
            self.getImages()
        }
        else {
            imagesArray.append(selectedPost.leftImage)
            imagesArray.append(selectedPost.rightImage)
            self.preparePageViewController()
        }
    }
    
    //MARK: - Rol Service Methods
    private func getImages(){
        rolService.convertImageFileToUIImage(self.selectedPost.rightImageFile) { (returnedImage:UIImage?, error:NSError?) -> Void in
            if let parseError = error {
                self.showErrorMessage(parseError)
            }
            else {
                self.selectedPost.rightImage = returnedImage
                self.imagesArray.append(returnedImage!)
                self.rolService.convertImageFileToUIImage(self.selectedPost.leftImageFile, completionFunction: { (returnedLeftImage:UIImage?, error:NSError?) -> Void in
                    if let parseError = error{
                        self.showErrorMessage(parseError)
                    }
                    else{
                        self.selectedPost.leftImage = returnedLeftImage
                        self.imagesArray.append(returnedLeftImage!)
                        self.preparePageViewController()
                    }
                })
            }
        }
    }
    
    private func deletePost(){

        rolService.deletePost(selectedPost) { (success:Bool?, error:NSError?) -> Void in
            if success == false {
                self.showAlertControllerWithTitle("Sorry!", message: "The post couldn't be deleted please try again.")
            }
        }
        NSNotificationCenter.defaultCenter().postNotificationName("postDeleted", object: nil)
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    private func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        pageController.dataSource = self
        
        if imagesArray!.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
        appearance.backgroundColor = UIColor.blackColor()
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageContentVC
        
        if itemController.pageIndex > 0 {
            return getItemController(itemController.pageIndex!-1)
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageContentVC
        
        if itemController.pageIndex!+1 < imagesArray!.count {
            return getItemController(itemController.pageIndex!+1)
        }
        
        return nil
    }
    
    private func getItemController(itemIndex: Int) -> PageContentVC? {
        
        if itemIndex < imagesArray!.count {
            
            let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("PageContentVC") as! PageContentVC
            pageItemController.pageIndex = itemIndex
            pageItemController.image = imagesArray![itemIndex]
            pageItemController.selectedPost = self.selectedPost
            if !isPostFromProfile {
                pageItemController.myProtocol = self.ratingDelegate
            }
            pageItemController.isPostFromProfile = self.isPostFromProfile
            
            return pageItemController
        }
        
        return nil
    }
    
    // MARK: - Page Indicator
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return imagesArray!.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    //MARK: - Private Methods
    
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func preparePageViewController(){
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        self.createPageViewController()
        self.setupPageControl()
    }
    
    @IBAction func trashButtonPressed(sender: UIBarButtonItem) {
        self.deletePost()
    }
}
